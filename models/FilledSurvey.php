<?php

namespace app\models;

use Yii;

use app\models\FilledAnswer;

/**
 * This is the model class for table "filled_survey".
 *
 * @property integer $id
 * @property integer $survey_id
 * @property integer $filled_by
 *
 * @property FilledAnswer[] $filledAnswers
 * @property User $filledBy
 * @property Survey $survey
 */
class FilledSurvey extends \yii\db\ActiveRecord
{

    public $filledAnswerContainer = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'filled_survey';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['survey_id', 'filled_by'], 'integer'],
            [['filled_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['filled_by' => 'id']],
            [['survey_id'], 'exist', 'skipOnError' => true, 'targetClass' => Survey::className(), 'targetAttribute' => ['survey_id' => 'id']],

            ['filled_by' , 'isAlreadyFilled'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'survey_id' => 'Survey ID',
            'filled_by' => 'Filled By',
        ];
    }

    /**
     * Checks if survey is filled by this person.
     *
     * @return void | error
     */
    public function isAlreadyFilled($attribute, $params, $validator)
    {
        $ifFilled = self::find()->where(['survey_id' => $this->survey_id, 'filled_by' => $this->$attribute])->one();

        if ($ifFilled) {
            $this->addError($attribute, 'This survey was already filled by you');
        }
    }
    

    /**
     * Validates all parameters
     *
     * @return boolean
     */
    public function validateAll()
    {
        $valid = $this->validate();
        foreach ($this->filledAnswerContainer as $filledAnswer) {
            $valid = $filledAnswer->validate() && $valid;
        }

        return $valid;
    }

    /**
     * SavesAll
     *
     * @return void
     */
    public function saveAll()
    {
        $this->save(false);
        $rows = [];
        foreach ($this->filledAnswerContainer as $filledAnswer) {
            $filledAnswer->filled_survey_id = $this->id;
        }

        $filledAnswer = new FilledAnswer;
        Yii::$app->db->createCommand()->batchInsert(FilledAnswer::tableName(), $filledAnswer->attributes(), $this->filledAnswerContainer)->execute();
    }
    
    

    /**
     * Add filled answer
     *
     * @return void
     */
    public function addFilledAnswer(\app\models\FilledAnswer $filledAnswer)
    {
        $this->filledAnswerContainer[] = $filledAnswer;
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilledAnswers()
    {
        return $this->hasMany(FilledAnswer::className(), ['filled_survey_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilledBy()
    {
        return $this->hasOne(User::className(), ['id' => 'filled_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurvey()
    {
        return $this->hasOne(Survey::className(), ['id' => 'survey_id']);
    }
}
