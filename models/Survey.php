<?php

namespace app\models;

use Yii;

use app\models\Answer;

/**
 * This is the model class for table "survey".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $created_by
 *
 * @property Question[] $questions
 * @property User $createdBy
 */
class Survey extends \yii\db\ActiveRecord
{
    public $questionContainer = [];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'survey';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            [['created_by'], 'integer'],
            [['name'], 'string', 'max' => 130],
            [['description'], 'string', 'max' => 500],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'created_by' => 'Created By',
        ];
    }

    /**
     * Adds question
     *
     * @return void
     */
    public function addQuestion(\app\models\Question $question)
    {
        $this->questionContainer[] = $question;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(Question::className(), ['survey_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilledSurveys()
    {
        return $this->hasMany(FilledSurvey::className(), ['survey_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }


    /**
     * validate survey
     *
     * @return boolean
     */
    public function validateAll()
    {
        $valid = $this->validate();

        foreach ($this->questionContainer as $question) {
            $valid = $valid && $question->validate();
            foreach ($question->answerContainer as $answer) {
                $valid = $valid && $answer->validate();
            }
        }

        return $valid;
    }

    /**
     * Save survey, all questions and answers
     *
     * @return void
     */
    public function saveAll()
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $this->save(false);
            $rows = [];
            foreach ($this->questionContainer as $question) {
                $question->survey_id = $this->id;
                $question->save(false);
                foreach ($question->answerContainer as $answer) {
                    $answer->question_id = $question->id;
                    $rows[] = $answer;
                }
            }

            $answer = new Answer;
            Yii::$app->db->createCommand()->batchInsert(Answer::tableName(), $answer->attributes(), $rows)->execute();
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
}
