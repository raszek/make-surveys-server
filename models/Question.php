<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "question".
 *
 * @property integer $id
 * @property string $question
 * @property integer $survey_id
 *
 * @property Answer[] $answers
 * @property Survey $survey
 */
class Question extends \yii\db\ActiveRecord
{

    public $answerContainer = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'question';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question'], 'required'],
            [['survey_id'], 'integer'],
            [['question'], 'string', 'max' => 130],
            [['survey_id'], 'exist', 'skipOnError' => true, 'targetClass' => Survey::className(), 'targetAttribute' => ['survey_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question' => 'Question',
            'survey_id' => 'Survey ID',
        ];
    }

    /**
     * Adding answer
     *
     * @return null
     */
    public function addAnswer(\app\models\Answer $answer)
    {
        $this->answerContainer[] = $answer;
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswers()
    {
        return $this->hasMany(Answer::className(), ['question_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurvey()
    {
        return $this->hasOne(Survey::className(), ['id' => 'survey_id']);
    }
}
