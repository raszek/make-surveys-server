<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "filled_answer".
 *
 * @property integer $id
 * @property integer $question_id
 * @property integer $answer_id
 * @property integer $filled_survey_id
 *
 * @property Question $question
 * @property Answer $answer
 * @property FilledSurvey $filledSurvey
 */
class FilledAnswer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'filled_answer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question_id', 'answer_id', 'filled_survey_id'], 'integer'],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => Question::className(), 'targetAttribute' => ['question_id' => 'id']],
            [['answer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Answer::className(), 'targetAttribute' => ['answer_id' => 'id']],
            [['filled_survey_id'], 'exist', 'skipOnError' => true, 'targetClass' => FilledSurvey::className(), 'targetAttribute' => ['filled_survey_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question_id' => 'Question ID',
            'answer_id' => 'Answer ID',
            'filled_survey_id' => 'Filled Survey ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Question::className(), ['id' => 'question_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswer()
    {
        return $this->hasOne(Answer::className(), ['id' => 'answer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilledSurvey()
    {
        return $this->hasOne(FilledSurvey::className(), ['id' => 'filled_survey_id']);
    }
}
