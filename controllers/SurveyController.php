<?php
namespace app\controllers;

use Yii;

use yii\filters\auth\HttpBearerAuth;
use yii\helpers\Json;

use app\models\User;
use app\models\Survey;
use app\models\Question;
use app\models\Answer;
use app\models\FilledSurvey;
use app\models\FilledAnswer;

class SurveyController extends \yii\rest\ActiveController
{
    public $modelClass = 'app\models\Survey';

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        // remove authentication filter
        $auth = $behaviors['authenticator'];
        unset($behaviors['authenticator']);

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
        ];

        // re-add authentication filter
        $behaviors['authenticator'] = $auth;

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
        ];

        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
        $behaviors['authenticator']['except'] = ['options'];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['create'], $actions['view'], $actions['index']);

        return $actions;
    }

    /**
     * Get list of surveys
     *
     * @return surveys
     */
    public function actionIndex()
    {
        $query = 'SELECT survey.id, survey.name, survey.description, user.email,
            exists(select 1 from filled_survey where filled_by=:id and survey.id=filled_survey.survey_id) as filled
            FROM survey
            join user on user.id=survey.created_by
            order by filled desc';

        $id = Yii::$app->user->id;
        $surveys = Yii::$app->db->createCommand($query)->bindParam(':id', $id)->queryAll();

        return $surveys;
    }
    


    /**
     * Fills a survey
     *
     * @return void
     */
    public function actionFillSurvey()
    {

        if ($data = Yii::$app->request->post()) {
            $filledSurvey = $this->loadAnswers($data);
            if ($filledSurvey->validateAll()) {
                $filledSurvey->saveAll();
                return true;
            }
            
        }

        return false;
    }


    /**
     * Loads filled answers for survey
     *
     * @return data
     */
    private function loadAnswers($data)
    {
        $newFilledSurvey = new FilledSurvey;
        $newFilledSurvey->survey_id = $data['surveyId'];
        $newFilledSurvey->filled_by = Yii::$app->user->id;
        foreach ($data['filledAnswers'] as $dataFilledAnswer) {
            $filledAnswer = new FilledAnswer;
            $filledAnswer->question_id = $dataFilledAnswer['questionId'];
            $filledAnswer->answer_id = $dataFilledAnswer['answerId'];
            $newFilledSurvey->addFilledAnswer($filledAnswer);
        }

        return $newFilledSurvey;
    }

    /**
     * Get info on whole survey
     *
     * @return json
     */
    public function actionView($id)
    {
        $survey = Survey::find()
            ->with(['questions' => function($query) {
                $query->with(['answers']);
            }])
            ->where(['id' => $id])
            ->asArray()
            ->one();

        return $survey;
    }
    

    /**
     * Add new survey.
     *
     * @return json
     */
    public function actionCreate()
    {

        if ($data = Yii::$app->request->post()) {
            $survey = $this->loadSurvey($data);
            if ($survey->validateAll()) {
                $survey->saveAll();

                return ['result' => 'success'];
            }
        }

        return ['result' => 'failure'];
    }

    /**
     * Loads survey
     *
     * @return data
     */
    private function loadSurvey($data)
    {
        $survey = new Survey;
        $survey->name = $data['name'];
        $survey->description = $data['description'];
        $survey->created_by = Yii::$app->user->id;
        foreach ($data['questions'] as $dataQuestion) {
            $question = new Question;
            $question->question = $dataQuestion['question'];
            foreach ($dataQuestion['answers'] as $dataAnswer) {
                $answer = new Answer;
                $answer->answer = $dataAnswer['answer'];
                $question->addAnswer($answer);
            }
            $survey->addQuestion($question);
        }

        return $survey;
    }
    
}
