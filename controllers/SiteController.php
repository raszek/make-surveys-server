<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\ContentNegotiator;
use yii\web\Response;

use app\models\User;
use app\models\Survey;
use app\components\AuthHandler;

class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Get results about each survey with one question
     *
     * @return surveys[]
     */
    public function actionSurveysOverview()
    {

        $sql = 
        'SELECT survey.id as survey_id, question.id as question_id, question.question, answer.answer, count(answer_id) as answer_count FROM answer
        left join filled_answer on filled_answer.answer_id=answer.id
        join question on answer.question_id=question.id
        join survey on question.survey_id=survey.id
        group by answer.id
        order by question.id';

        $data = Yii::$app->db->createCommand($sql)->queryAll();

        if (count($data) == 0) {
            return [];
        }

        $surveyResults = [];
        foreach ($data as $row) {
            $surveyResults[$row['question_id']]['surveyId'] = $row['survey_id'];
            $surveyResults[$row['question_id']]['question'] = $row['question'];
            $surveyResults[$row['question_id']]['answers'][] = $row['answer'];
            $surveyResults[$row['question_id']]['answerCounts'][] = $row['answer_count'];
        }

        $randomQuestions = [];
        $surveyResults = array_values($surveyResults);
        $maxQuestions = floor(count($surveyResults)/2);
        $randomIndexes = array_rand($surveyResults, $maxQuestions);
        foreach ($randomIndexes as $index) {
            $randomQuestions[] = $surveyResults[$index];
        }

        return $randomQuestions;
    }

    /**
     * Get summarized survey results
     *
     * @return data
     */
    public function actionSurveyResults($id)
    {

        $sql = 
        'SELECT question.id as question_id, question.question, answer.answer, count(answer_id) as answer_count FROM answer
        left join filled_answer on filled_answer.answer_id=answer.id
        join question on answer.question_id=question.id
        join survey on question.survey_id=survey.id
        where survey.id=:id
        group by answer.id
        order by question.id';

        $data = Yii::$app->db->createCommand($sql)->bindParam(':id', $id)->queryAll();

        $surveyResults = [];
        foreach ($data as $row) {
            $surveyResults[$row['question_id']]['question'] = $row['question'];
            $surveyResults[$row['question_id']]['answers'][] = $row['answer'];
            $surveyResults[$row['question_id']]['answerCounts'][] = $row['answer_count'];
        }

        return array_values($surveyResults);
    }
    
    

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $identity = User::findOne(['email' => $model->email]);
            if ($identity->validatePassword($model->password_hash)) {
                $loginData['email'] = $identity->email;
                $loginData['JWT'] = $identity->getJWT();
                
                return $loginData;
            }
        }

        return ['errMsg' => 'Invalid credentials'];
    }

}
